import 'package:flutter/material.dart';

class NewTransaction extends StatelessWidget {
  // String titleInput;
  // String amountInput;

  final titleInput = TextEditingController();
  final amountInput = TextEditingController();
  final Function addNewTx;

  NewTransaction(this.addNewTx);

  void submitData() {
    final enteredTitle = titleInput.text;
    final enteredAmount = double.parse(amountInput.text);

    if (enteredTitle.isEmpty ||
        enteredAmount.isNaN ||
        enteredAmount.isNegative) {
      print(
          'entered value doesnt meet conditions: $enteredAmount, and or $enteredTitle');
      return;
    }

    addNewTx(
      enteredTitle,
      enteredAmount,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextField(
              decoration: InputDecoration(labelText: 'Title'),
              keyboardType: TextInputType.text,
              // onSubmitted: submitData, //cant do just submitData - uncoment and see why
              onSubmitted: (_) =>
                  submitData(), //we have to use anonymous func with value which we dont care of therefore '_'
              // onChanged: (val) => titleInput = val,
              controller: titleInput,
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Amount'),
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              // onSubmitted: submitData, //cant do just submitData - uncoment and see why
              onSubmitted: (_) =>
                  submitData(), //we have to use anonymous func with value which we dont care of therefore '_'
              // onChanged: (val) {
              //   amountInput = val;
              // },
              controller: amountInput,
            ),
            TextButton(
              child: Text('Add Transaction'),
              onPressed: submitData,
            ),
          ],
        ),
      ),
    );
  }
}
